#!/usr/bin/python3

#========== Kim Chu's Law of Classroom ===============#

# KIM: SA CLASSROOM MAY BATAS
with_law = input("Do want to implement Kim Chu's Law of Classroom in this room? Press [y] if yes: ") 
if with_law.lower() == 'y':

	# create loop as long as 'can_go_out' is False
	# BAWAL LUMABAS, OH BAWAL LUMABAS.
	can_go_out = False
	while can_go_out is not True:

		# PERO KAPAG NAG COMPLY KA NA BAWAL LUMABAS
		comply = input("Do you comply to the Kim Chu's Law of Classroom? Press [y] if yes: ")
		if comply.lower() == 'y' and can_go_out is False:

			# PERO MAY GINAWA KA SA PINAGBABAWAL NILA
			did_something_prohibited = input("Did you do something prohibited in the classroom? Press [y] if yes: ")
			if did_something_prohibited.lower() == 'y':

				# Define status default values
				fix_status = ''
				submit_status = False

				# Loop the process if 'fix_status' or 'submit_status' variables are not 'y'
				while fix_status.lower() != 'y' or submit_status is False:

					# INAYOS MO UNG LAW NG CLASSROOM NYO
					fix_status = input("Do want to fix the law of this classroom? Press [y] if yes: ")
					if fix_status.lower() == 'y':

						# AT SINUBMIT MO ULET
						while not submit_status:
							s_stat = input("Do you do want to submit changes? Press [y] if yes: ")
							if s_stat.lower() == 'y':
								submit_status = True
								can_go_out = True
							else:
								print("You must submit the changes!")
					else:
						print("Please fix classroom's law.")
				# if both 'fix_status' and 'submit_status' condition are satisfied, we can exit the loop
			else:
				print("You didn't do anything prohibited. You must be a good student.")
				can_go_out = True
		else:
			print("You must comply to Kim Chu's Law of Classroom")
	else:
		# AIY, PWEDE NA PALA IKAW LUMABAS
		print("Congrats! Pde na ikaw lumabas :)")
else:
	print("Please implement Kim Chu's Law of Classroom")
	
